﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testTenis
{
    class Match
    {
        public Random r = new Random();
        public Player jugadorA;
        public Player jugadorB;
        public int numset;
        public int pa=0;
        public int pb=0;        
        public string A = "0";
        public string B = "0";
        public int puntosSetA = 0;
        public int puntosSetB = 0;
        public int setActual = 0;
        public int juego;
        public string resultado="";
        public string ganador="";
        public string puntos="";
        public string resultadoSet = "";
        string r1;
        string r2;
        string r3;
        string r4;
        string r5;
        string iniciaJuego = "";
        
        //creo unos arrays para guardar los resultados
        string[] resulJuegos = new string[0];
        
        string[] resultados = new string[0];
        string[] ganadorSet = new string[0];
        int[] setS = new int[0];


        /// <summary>
        /// Clase Match
        /// </summary>
        /// <param name="jA"></param>
        /// <param name="jB"></param>
        /// <param name="numset"></param>
        public Match(Player jA, Player jB, int numset)
        {
            jugadorA = jA;
            jugadorB = jB;
            this.numset = numset;

            menu();
        }

        /// <summary>
        /// método para mostrar el menú
        /// </summary>
        public void menu()
        {
            int opcion = 0;
            do
            {
                bienvenida();

                Console.WriteLine("0.- Salir de la aplicación");
                Console.WriteLine("1.- Comenzar el juego");
                Console.WriteLine("2.- Mostrar los nombres de los jugadores");
                Console.WriteLine("3.- Mostrar los resultados del juego");
                Console.WriteLine("4.- Mostrar los resultados de los sets");
                Console.WriteLine("----------------------------------------");
                Console.WriteLine();
                Console.Write("Elige una opcion: ");
                try
                {
                    opcion = int.Parse(Console.ReadLine());
                }
                catch 
                {

                    Console.WriteLine("No has introducido un número. Pulsa una tecla para continuar e inténtalo de nuevo");
                    opcion = int.Parse(Console.ReadLine());
                }

                if (opcion >= 5)
                {
                    Console.WriteLine("No es una opción correcta. Pulsa una tecla para continuar e inténtalo de nuevo");
                    Console.ReadKey();
                }

                switch (opcion)
                {
                    case 1:                       
                        
                        if (resulJuegos.Length > 0)
                        {
                            Console.WriteLine("Ya has jugado el partido");
                            Console.Write("Pulsa una tecla para continuar ");
                            Console.ReadKey();
                        }
                        else
                        {
                            jugar();
                            Console.Write("Pulsa una tecla para continuar ");
                            Console.ReadKey();
                        }                        
                        break;

                    case 2:                                      
                        mostrarNombres();
                        break;

                    case 3:
                        if (resulJuegos.Length > 0)
                        {
                            mostrarJuegos();
                        }else
                        {
                            Console.WriteLine("Todavía no has jugado el partido. No puedo mostrarte resultados del partido");
                            Console.Write("Pulsa una tecla para continuar ");
                            Console.ReadKey();
                        }
                           
                        break;

                    case 4:
                        if (resulJuegos.Length > 0)
                        {
                            mostrarSet();
                        }
                        else
                        {
                            Console.WriteLine("Todavía no has jugado el partido. No puedo mostrarte resultados de los sets");
                            Console.Write("Pulsa una tecla para continuar ");
                            Console.ReadKey();
                        }

                        break;
                }

            } while (opcion != 0);            
            Environment.Exit(0); //salgo de la aplicación si la opción elegida es 0
        }

        /// <summary>
        /// método para mostrar la bienvenida
        /// </summary>
        public void bienvenida()
        {
            Console.Clear();
            Console.WriteLine("Bienvenido al torneo de tenis de Vueling");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine();
        }

        /// <summary>
        /// método para realizar la simulación del partido
        /// </summary>
        public void jugar()
        {
            bienvenida();
            int saca = r.Next(2);
            if (saca == 0)
            {
                Console.WriteLine("El jugador " + jugadorA.nombre + " inicia el partido");
                iniciaJuego = jugadorA.nombre;
            }
            else
            {
                Console.WriteLine("El " + jugadorB.nombre + " inicia el partido");
                iniciaJuego = jugadorB.nombre;
            }

            // inicializo a 1 la variable setActual
            setActual = 1;
           
            //realizaré el juego mientras el set actual sea menor o igual al numero de sets introducidos
            while (setActual <= numset)
            {
                Console.WriteLine("Set " + setActual);
                juego = 0;
                while(pa<=7 || pb <= 7)
                {
                    juego++;                    
                    resultado= "Juego " + juego;
                    Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                    resulJuegos[resulJuegos.Length - 1] = resultado;
                    Console.WriteLine(resultado);
                    jugarJuego();
                    
                    if (pa == 7) 
                    // si el jugador A tiene 7 puntos, ha desempatado y ganado el juego y el set.
                    {                        
                        ganador = jugadorA.nombre;
                        guardarSet();
                        resultado = "Set para " + jugadorA.nombre;
                        Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                        resulJuegos[resulJuegos.Length - 1] = resultado;
                        Console.WriteLine(resultado);
                        resultado = "----------------------";
                        Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                        resulJuegos[resulJuegos.Length - 1] = resultado;
                        Console.WriteLine(resultado);
                        break;
                    }else if (pb == 7) 
                    // si el jugador B tiene 7 puntos, ha desempatado y ganado el juego y el set.
                    {                        
                        ganador = jugadorB.nombre;
                        guardarSet();
                        resultado = "Set para " + jugadorB.nombre;
                        Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                        resulJuegos[resulJuegos.Length - 1] = resultado;
                        Console.WriteLine(resultado);
                        resultado = "----------------------";
                        Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                        resulJuegos[resulJuegos.Length - 1] = resultado;
                        Console.WriteLine(resultado);
                        break;
                    }else if (pa == 6) 
                    //si el jugador A tiene 6 puntos y la diferencia con el jugador B es igual o mayor a 2, gana el set
                    //de lo contrario, se sigue jugando
                    {
                        if (pa >= pb + 2)
                        {                            
                            puntosSetA++;
                            ganador = jugadorA.nombre;
                            guardarSet();
                            resultado = "Set para " + jugadorA.nombre;
                            Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                            resulJuegos[resulJuegos.Length - 1] = resultado;
                            Console.WriteLine(resultado);
                            resultado = "----------------------";
                            Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                            resulJuegos[resulJuegos.Length - 1] = resultado;
                            Console.WriteLine(resultado);
                            break;
                        }

                    }else if(pb == 6)
                    //si el jugador B tiene 6 puntos y la diferencia con el jugador A es igual o mayor a 2, gana el set
                    //de lo contrario, se sigue jugando
                    {
                        if (pb >= pa + 2)
                        {                            
                            puntosSetB++;
                            ganador = jugadorB.nombre;
                            guardarSet();
                            resultado = "Set para " + jugadorB.nombre;
                            Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                            resulJuegos[resulJuegos.Length - 1] = resultado;
                            Console.WriteLine(resultado);
                            resultado = "----------------------";
                            Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                            resulJuegos[resulJuegos.Length - 1] = resultado;
                            Console.WriteLine(resultado);
                            break;
                        }
                    }
                }
                setActual++;
                pa = 0;
                pb = 0;
                
            }
            if (puntosSetA > puntosSetB)
            {
                resultado = "GANA EL PARTIDO " + jugadorA.nombre;
                Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                resulJuegos[resulJuegos.Length - 1] = resultado;
                Console.WriteLine(resultado);
                //Console.WriteLine("GANA EL PARTIDO EL JUGADOR "+jugadorA.nombre);
            }
            else
            {
                resultado = "GANA EL PARTIDO " + jugadorB.nombre;
                Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                resulJuegos[resulJuegos.Length - 1] = resultado;
                Console.WriteLine(resultado);
               // Console.WriteLine("GANA EL PARTIDO EL JUGADOR "+ jugadorB.nombre);
            }           
        }

        /// <summary>
        /// método para realizar la simulación de cada juego
        /// </summary>
        public void jugarJuego()
        {
            //inicio el juego del set actual
            int punto = 0;
            
            do
            {
                punto++;
                int puntua = r.Next(2);

                if (puntua == 0)
                {
                    //si puntua =0 asigno los puntos al jugador A
                    if (A == "40")
                    {
                        //si A tiene 40 puntos, compruebo los puntos que tiene B
                        if (B == "40")
                        {
                            //desempato
                            desempate();                            
                            A = "0";
                            B = "0";                                 
                            break;
                        }
                        else
                        {
                            //gana el punto A
                            A = "Juego";
                            pa++;                           
                            puntos = jugadorA.nombre;                            
                            guardarJuego();     
                            
                            //inicializo los valores de A y B para el siguiente juego
                            A = "0";
                            B = "0";
                            break;
                        }
                    }
                    else if (A == "30")
                    {
                        A = "40";
                        puntos = jugadorA.nombre;
                        guardarJuego();
                    }
                    else if (A == "15")
                    {
                        A = "30";
                        puntos = jugadorA.nombre;
                        guardarJuego();
                    }
                    else
                    {
                        A = "15";
                        puntos = jugadorA.nombre;
                        guardarJuego();
                    }
                }
                else
                {
                    //si puntua =1 asigno los puntos al jugador B
                    if (B == "40")
                    {
                        if (A == "40")
                        {
                            //desempato
                            desempate();
                            A = "0";
                            B = "0";                            
                            break;
                        }
                        else
                        {
                            B = "Juego";
                            pb++;
                            puntos = jugadorB.nombre;
                            guardarJuego();
                            //inicializo los valores de A y B para el siguiente juego
                            A = "0";
                            B = "0";
                            break;
                        }
                    }
                    else if (B == "30")
                    {
                        B = "40";
                        puntos = jugadorB.nombre;
                        guardarJuego();
                    }
                    else if (B == "15")
                    {
                        B = "30";
                        puntos = jugadorB.nombre;
                        guardarJuego();
                    }
                    else
                    {
                        B = "15";
                        puntos = jugadorB.nombre;
                        guardarJuego();
                    }
                }                 
            } while (punto <= 7);
        }

        /// <summary>
        /// método para desempatar un juego
        /// </summary>
        public void desempate()
        {
            do
            {
                //desempato
                int desempate = r.Next(2);
                if (desempate == 0)
                {
                    if (A == "Ad")
                    {
                        //gana el juego el jugador A
                        A = "Juego";
                        pa++;
                        puntos = jugadorA.nombre;
                        guardarJuego();
                        //reinicio valores de A y B para el siguiente juego
                        A = "0";
                        B = "0";
                        break;
                    }
                    else if (B == "Ad")
                    {
                        //iguales
                        A = "40";
                        B = "40";
                        puntos = jugadorA.nombre;
                        guardarJuego();
                    }
                    else
                    {
                        //ventaja para A
                        A = "Ad";
                        B = "40";
                        puntos = jugadorA.nombre;
                        guardarJuego();
                    }
                }
                else
                {                   
                    if (B == "Ad")
                    {
                        //gana el juego el jugador B
                        B = "Juego";
                        pb++;
                        puntos = jugadorB.nombre;
                        guardarJuego();
                        //reinicio valores de A y B para el siguiente juego
                        A = "0";
                        B = "0";
                        break;
                    }
                    else if (A == "Ad")
                    {
                        //iguales
                        A = "40";
                        B = "40";
                        puntos = jugadorB.nombre;
                        guardarJuego();
                    }
                    else
                    {
                        //ventaja para B
                        B = "Ad";
                        A = "40";
                        puntos = jugadorB.nombre;
                        guardarJuego();
                    }
                } //fin de  if (desempate == 0)            

            } while (A != "Juego" || B != "Juego");           
        }

        /// <summary>
        /// método para guardar los resultados de los juegos
        /// </summary>
        public void guardarJuego()
        {
            Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
            resultado = "Puntúa " + puntos + " " + A + "-" + B + "\t\t\t ";

            if (numset == 3)
                //si el numero de sets es 3, guardaré el resultador con el formato (0 - 0), (0 - 0), (0 - 0)
                {
                if (setActual == 1)
                {
                    r1 = "(" + pa + "-" + pb + ")";
                    r2 = "(0-0)";
                    r3 = "(0-0)";
                }
                else if (setActual == 2)
                {
                    r2 = "(" + pa + "-" + pb + ")";
                    r3 = "(0-0)";
                }
                else if (setActual == 3)
                {
                    r3 = "(" + pa + "-" + pb + ")";
                }
                resultado += r1 + ", " + r2 + ", " + r3;
            }
            else if (numset == 5)
            //si el numero de sets es 5, guardaré el resultador con el formato (0 - 0), (0 - 0), (0 - 0), (0 - 0), (0 - 0)
            {
                if (setActual == 1)
                {
                    r1 = "(" + pa + "-" + pb + ")";
                    r2 = "(0-0)";
                    r3 = "(0-0)";
                    r4 = "(0-0)";
                    r5 = "(0-0)";
                }
                else if (setActual == 2)
                {
                    r2 = "(" + pa + "-" + pb + ")";
                    r3 = "(0-0)";
                    r4 = "(0-0)";
                    r5 = "(0-0)";
                }
                else if (setActual == 3)
                {
                    r3 = "(" + pa + "-" + pb + ")";
                    r4 = "(0-0)";
                    r5 = "(0-0)";
                }
                else if (setActual == 4)
                {
                    r4 = "(" + pa + "-" + pb + ")";
                    r5 = "(0-0)";
                }
                else
                {
                    r5 = "(" + pa + "-" + pb + ")";
                }
                resultado += r1 + ", " + r2 + ", " + r3 + ", " + r4 + ", " + r5;
            }

            resulJuegos[resulJuegos.Length - 1] = resultado;

            Console.WriteLine(resultado);

            if (A == "Juego")
            {
                Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                resultado = "Juego para " + puntos;

                resulJuegos[resulJuegos.Length - 1] = resultado;
                Console.WriteLine(resultado);
                Console.WriteLine();
            }
            else if (B == "Juego")
            {
                Array.Resize(ref resulJuegos, resulJuegos.Length + 1);
                resultado = "Juego para " + puntos;

                resulJuegos[resulJuegos.Length - 1] = resultado;
                Console.WriteLine(resultado);
                Console.WriteLine();
            }          
        }

        /// <summary>
        /// método para guardar los resultados de los sets
        /// </summary>
        public void guardarSet()
        {
            if (numset == 3)
            //si el numero de sets es 3, guardaré el resultador con el formato (0-0),(0-0),(0-0)
            {
                if (setActual == 1)
                {
                    r1 = "(" + pa + "-" + pb + ")";
                    r2 = "(0-0)";
                    r3 = "(0-0)";

                }
                else if (setActual == 2)
                {
                    r2 = "(" + pa + "-" + pb + ")";
                    r3 = "(0-0)";
                }
                else if (setActual == 3)
                {
                    r3 = "(" + pa + "-" + pb + ")";
                }
                resultadoSet = r1 + ", " + r2 + ", " + r3;
            }
            else if(numset==5)
            //si el numero de sets es 5, guardaré el resultador con el formato (0-0),(0-0),(0-0),(0-0),(0-0)
            {
                if (setActual == 1)
                {
                    r1 = "(" + pa + "-" + pb + ")";
                    r2 = "(0-0)";
                    r3 = "(0-0)";
                    r4 = "(0-0)";
                    r5 = "(0-0)";
                }
                else if (setActual == 2)
                {
                    r2 = "(" + pa + "-" + pb + ")";
                    r3 = "(0-0)";
                    r4 = "(0-0)";
                    r5 = "(0-0)";
                }
                else if (setActual == 3)
                {
                    r3 = "(" + pa + "-" + pb + ")";
                    r4 = "(0-0)";
                    r5 = "(0-0)";
                }
                else if (setActual == 4)
                {
                    r4 = "(" + pa + "-" + pb + ")";                   
                    r5 = "(0-0)";
                }
                else if(setActual==5)
                {
                    r5 = "(" + pa + "-" + pb + ")";
                }
               resultadoSet = r1+", "+r2 + ", " + r3 + ", " + r4 + ", " + r5;
            }          

            Array.Resize(ref ganadorSet, ganadorSet.Length + 1);
            Array.Resize(ref resultados, resultados.Length + 1);
            Array.Resize(ref setS, setS.Length + 1);

            ganadorSet[ganadorSet.Length - 1] = ganador;
            resultados[resultados.Length - 1] = resultadoSet;
            setS[setS.Length - 1] = setActual;
        }
          
        /// <summary>
        /// método para mostrar el nombre de los jugadores
        /// </summary>
        public void mostrarNombres()
        {
            bienvenida();
            Console.WriteLine("has elegido mostrar lo nombres de los jugadores");  
            Console.WriteLine();
            Console.WriteLine("El nombre del primer jugador es " + jugadorA.nombre);
            Console.WriteLine("El nombre del segundo jugador es " + jugadorB.nombre);
            Console.WriteLine();
            Console.Write("Pulsa una tecla para continuar");
            Console.ReadKey();
        }

        /// <summary>
        /// método para mostrar el resultado de un set
        /// </summary>
        public void mostrarSet()
        {
            bienvenida();

            Console.WriteLine("has elegido mostrar los resultados de los sets");
            Console.WriteLine();
            Console.WriteLine("El partido lo ha iniciado "+ iniciaJuego);
            Console.WriteLine();
            int n = 0;
           
            for (int i = 0; i <= ganadorSet.Length; i++)
            {     
                try
                {
                    if (ganadorSet[i] == jugadorA.nombre)
                    {
                        n++;
                    }
                    
                    string r = resultados[i];
                    
                    Console.WriteLine(ganadorSet[i] + " ha ganado el set " + (i + 1) + ". " + r);
                }
                catch (Exception)
                {
                                      
                }
            }
            Console.WriteLine();
            if (numset == 3)
            {
                if (n >= 2)
                {
                    Console.WriteLine("GANA EL PARTIDO "+jugadorA.nombre);
                }else
                {
                    Console.WriteLine("GANA EL PARTIDO " + jugadorB.nombre);
                }
            }else if (numset == 5)
            {
                if (n >= 3)
                {
                    Console.WriteLine("GANA EL PARTIDO " + jugadorA.nombre);
                }
                else
                {
                    Console.WriteLine("GANA EL PARTIDO " + jugadorB.nombre);
                }
            }
            Console.WriteLine();
            Console.Write("Pulsa una tecla para continuar");
            Console.ReadKey();
        }

        /// <summary>
        /// método para mostrar los resultados de los juegos
        /// </summary>
        public void mostrarJuegos()
        {
            bienvenida();
            Console.WriteLine();
            Console.WriteLine("Has elegido mostrar los resultados de los juegos");
            Console.WriteLine();

            for (int j = 0; j < resulJuegos.Length; j++)
            {

                resultado = resulJuegos[j];
                if (resultado.Contains("Set para ")|| resultado.Contains("Juego para "))
                {
                    Console.WriteLine(resulJuegos[j]);
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine(resulJuegos[j]);
                }
                
            }
            //for (int j = 0; j < resulJuegos.Length; j++)
            //{
            //    resultado = resulJuegos[j];
            //    Console.WriteLine(resulJuegos[j]);
            //}
            Console.WriteLine();
            Console.WriteLine("Pulsa una tecla para continuar");
            
            Console.ReadKey();
        }
    }
}
