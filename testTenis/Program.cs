﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testTenis
{
    class Program
    {
        /// <summary>
        /// Crear un programa que pida por consola los nombres de los dos jugadores.
        /// Pedir por consola si el partido será a 3 ó 5 sets.
        /// Se haga un sorteo inicial para saber que jugador saca.
        /// Se juegue el partido con las reglas de puntuación definidas en https://es.wikipedia.org/wiki/Tenis#Puntuaci.C3.B3n
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //declaro variables que necesito
            string jugadorA;
            string jugadorB;
            int numset;

            Console.Clear();
            Console.WriteLine("Bienvenido al torneo de tenis de Vueling");
            Console.WriteLine("----------------------------------------");
            Console.WriteLine();

            //pido el nombre para el primer jugador, controlando que no sean caracteres en blanco
            do
            {
                Console.Write("Introduzca el nombre del primer jugador: ");
                jugadorA = Console.ReadLine();

            } while (jugadorA == (""));
            Player j1 = new Player(jugadorA);


            //pido el nombre del segundo jugador, controlando que no sean caracteres en blanco ni sea igual al nombre del jugador 1
            do
            {
                Console.Write("Introduzca el nombre del segundo jugador: ");
                jugadorB = Console.ReadLine();
                while (jugadorB.Equals(jugadorA))
                {
                    Console.Write("Los nombres de ambos jugadores tienen que ser distintos. Por favor, introduce otro nombre para el jugador 2: ");
                    jugadorB = Console.ReadLine();
                }

            } while (jugadorB == (""));
            Player j2 = new Player(jugadorB);

            //pido el número de sets que se van a jugar, comprobando que sea 3 o 5 el número introducido        
            do
            {
                Console.Write("¿Cuanto sets se van a jugar? (3 o 5) ");
                numset = int.Parse(Console.ReadLine());
                if (numset != 3 && numset != 5)
                {
                    Console.WriteLine("Sólo se pueden jugar 3 o 5 sets.");

                }
            } while (numset != 3 && numset != 5);
            
            //creo el partido de tenis con el jugador1, jugador2 y numset
            Match partido = new Match(j1, j2, numset);

            //llamo al método menú para mostrar el menú
            partido.menu();
        }
    }
}
